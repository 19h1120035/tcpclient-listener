﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.IO;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Tcp Client";
            IPAddress ip = IPAddress.Parse("127.0.0.1");
            while (true)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write("# Text >> ");
                Console.ResetColor();
                string text = Console.ReadLine();
                TcpClient client = new TcpClient();
                client.Connect(ip, 1308);
                NetworkStream stream = client.GetStream();
                StreamWriter sw = new StreamWriter(stream) { AutoFlush = true };
                StreamReader sr = new StreamReader(stream);
                sw.WriteLine(text);
                if (text == "exit") break;
                string response = sr.ReadLine();
                client.Close();
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write("$ Server >>> ");
                Console.ResetColor();
                Console.Write(response);
                Console.WriteLine();
            }
        }
    }
}
