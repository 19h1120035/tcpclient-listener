﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.IO;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Tcp Server";
            TcpListener listener = new TcpListener(IPAddress.Any, 1308);
            listener.Start(10);
            while (true)
            {
                TcpClient client = listener.AcceptTcpClient();
                NetworkStream stream = client.GetStream();
                StreamReader sr = new StreamReader(stream);
                StreamWriter sw = new StreamWriter(stream) { AutoFlush = true };
                string text = sr.ReadLine();
                if (text == "exit") break;
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write("# Client >>> ");
                Console.ResetColor();
                Console.Write(text);
                Console.WriteLine();
                string response = text.ToUpper();
                sw.WriteLine(response);
                client.Close();
            }
        }
    }
}
